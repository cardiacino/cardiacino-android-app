package com.rumaanapps.cardiacino.models;

import com.google.gson.Gson;

public class Pulses {
    private int val;
    private int bpm;
    private int ibi;

    private Pulses(){

    }

    public static Pulses fromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, Pulses.class);
    }

    public int getValue(){
        return this.val;
    }
    public int getBPM(){
        return this.bpm;
    }
    public int getHRV(){
        return this.ibi;
    }
}
