package com.rumaanapps.cardiacino;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("About");
        setContentView(R.layout.activity_about);
        TextView textView = (TextView) findViewById(R.id.desc_text);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(R.string.app_desc);
    }

    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }
}
