package com.rumaanapps.cardiacino;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ReportActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnShare , btnsubmit;
    int age;
    EditText ageInput;
    Intent shareIntent;
    String shareBody = "HELLO!! THIS IS A MESSAGE FROM REPORT ACTIVITY!!";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("Report");
        ((TextView)findViewById(R.id.avgbpm_text)).setText(String.valueOf(Util.getAvgBPM()));

        ageInput= (EditText) findViewById(R.id.age_edittext);

        btnsubmit=(Button) findViewById(R.id.submit_button) ;
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                age=Integer.valueOf(ageInput.getText().toString());
                showToast(String.valueOf(age));
            }
        });

        FloatingActionButton fab;
        fab = (FloatingActionButton) findViewById(R.id.graph_button);
        fab.setOnClickListener(this);

        btnShare = (Button) findViewById(R.id.share_button);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT,"CARDIACINO");
                shareIntent.putExtra(Intent.EXTRA_TEXT,shareBody);
                startActivity(Intent.createChooser(shareIntent,"share via..."));
            }
        });
    }

    @Override
    public void onBackPressed(){
        this.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == android.R.id.home){
            finish();
            return true;
        }
        return false;
    }

    public void onClick(View view) {
        ((TextView)findViewById(R.id.avgbpm_text)).setText(String.valueOf(Util.getAvgBPM()));

        int id = view.getId();

        if(id == R.id.graph_button){
            Intent graph = new Intent(this, GraphActivity.class);
            startActivity(graph);
        }
    }

    private void showToast(String text){
        Toast.makeText(ReportActivity.this,text,Toast.LENGTH_SHORT).show();
    }
}
