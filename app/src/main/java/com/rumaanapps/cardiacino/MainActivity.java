package com.rumaanapps.cardiacino;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rumaanapps.cardiacino.models.Pulses;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {


    private static int BPM = 0, HRV = 0;
    private boolean connected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab;
        fab = (FloatingActionButton) findViewById(R.id.report_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openReportActivity();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void openReportActivity()
    {
        Intent report = new Intent(this, ReportActivity.class);
        startActivity(report);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        //if (id == R.id.exit_action)
        switch (item.getItemId()){
            case R.id.exit_action:
                finish();
                break;
            case R.id.share_button:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "HELLO!! THIS IS A MESSAGE FROM CARDIACINO";
                String shareSubject = "HELLO!! THIS IS A MESSAGE FROM CARDIACINO";

                sharingIntent.putExtra(Intent.EXTRA_TEXT,shareBody);
                sharingIntent.putExtra(Intent.EXTRA_TEXT,shareSubject);

                startActivity(Intent.createChooser(sharingIntent,"share using"));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_about) {
            Intent about = new Intent(this, AboutActivity.class);
            startActivity(about);
        } else if (id == R.id.nav_exit) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static synchronized void setBPM(int bpm) {
        BPM = bpm;
    }
    public static synchronized void setHRV(int hrv) {
        HRV = hrv;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        if (id == R.id.press_button) {
            // Press Button action stuff here
            if (!connected) {
                Updater u = new Updater(this);
                u.execute();
            } else {
                Toast.makeText(this.getApplicationContext(), "Disconnecting!", Toast.LENGTH_SHORT).show();
                ((Button) findViewById(R.id.press_button)).setEnabled(false);
                connected = false;
            }
        } else if(id == R.id.text_bpm){
            Toast.makeText(findViewById(R.id.text_bpm).getContext(), "Beats Per Minute", Toast.LENGTH_SHORT).show();
        } else if (id == R.id.text_hrv) {
            Toast.makeText(findViewById(R.id.text_hrv).getContext(), "Heart Rate Variability (Inter Beat Interval)", Toast.LENGTH_SHORT).show();
        }
    }

    private class Updater extends AsyncTask<Void, Void, Boolean> {
        Activity act;
        private Updater(Activity activity) {
            act = activity;
        }

        public Boolean doInBackground(Void... vals) {
            Socket sock;
            if ((sock = Util.setupConnection(act)) != null) {
                connected = true;
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
                    while (sock.isConnected() && connected) {
                        if (Util.pause) continue;
                        String line = reader.readLine();
                        System.out.println(line);
                        final Pulses pulses = Pulses.fromJson(line);
                        setBPM(pulses.getBPM());
                        setHRV(pulses.getHRV());
                        Util.bpm.push(pulses.getBPM());
                        if(Util.bpm.size()>10)Util.bpm.remove(0);
                        act.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((TextView) findViewById(R.id.text_bpm)).setText(String.valueOf(pulses.getBPM()));
                                ((TextView) findViewById(R.id.text_hrv)).setText(String.valueOf(pulses.getHRV()));
                            }
                        });
                        Thread.sleep(500);
                    }
                } catch (Exception ex) {
                    Util.toast(act, "Corrupt reading values!");
                    return false;
                } finally {
                    try {
                        if (reader != null) reader.close();
                        sock.close();
                        act.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((Button) findViewById(R.id.press_button)).setEnabled(true);
                            }
                        });
                    } catch (IOException ex) {
                    }
                    connected = false;
                }
                return true;
            }
            return false;
        }
    }
}
