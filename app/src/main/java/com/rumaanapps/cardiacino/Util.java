package com.rumaanapps.cardiacino;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Stack;

public class Util
{
    public static java.util.Stack<Integer> bpm = new Stack<>();
    public static boolean pause = false;

    public static Socket setupConnection(Activity context) {
        ConnectivityManager cm = (ConnectivityManager)context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        WifiManager wm = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if(wm.isWifiEnabled()){
            WifiInfo wi = wm.getConnectionInfo();
            if(wi.getNetworkId()!=-1){
                try {
                    Socket sock = new Socket("192.168.4.1", 80);
                    PrintWriter out = new PrintWriter(sock.getOutputStream());
                    out.print("GET / HTTP/1.1\r\n\r\n");
                    out.flush();
                    return sock;
                } catch(UnknownHostException ex) {
                    toast(context, "The device unreachable!");
                    return null;
                } catch(IOException ex) {
                    toast(context, "Unable to communicate with the device!");
                    return null;
                }
            } else {
                toast(context, "Not Connected to the device!");
                return null;
            }
        } else {
            toast(context, "Turn on WIFI and connect to the device!");
            return null;
        }
    }

    public static void toast(final Activity act, final String msg){
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(act, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    public static int getAvgBPM() {
        if(bpm.size()<1) return 0;
        pause = true;
        int sum = 0, size = bpm.size();
        Integer[] vals = bpm.toArray(new Integer[size]);
        for(int i : vals) {
            sum += i;
        }
        pause = false;
        System.out.println(Arrays.deepToString(vals)+"\nAvg: "+(sum/size));
        return (sum/size);
    }
}
